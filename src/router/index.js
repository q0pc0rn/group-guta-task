import Vue from 'vue'
import Router from 'vue-router'
import Main from "../pages/Main";
import SubscribeDone from "../components/SubscribeDone";

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: '/group-guta-task/',
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/menu1',
      name: 'menu1'
    },
    {
      path: '/menu2',
      name: 'menu2'
    },
    {
      path: '/menu3',
      name: 'menu3'
    },
    {
      path: '/menu4',
      name: 'menu4'
    },
    {
      path: '/menu5',
      name: 'menu5'
    },
    {
      path: '/menu6',
      name: 'menu6'
    },
    {
      path: 'subscribe-done',
      name: 'subscribe-done',
      component: SubscribeDone
    }
  ]
});